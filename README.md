# figmental

## Nuxt Component Playground

### CLI

`figmental [root dir] [preview dir ] [include folders]`

1. root dir - Root directory of project, defaults to current working dir.
2. preview dir - Path to generated preview, relative to root, defaults to `"pages/preview"`
3. include folders - Comma seperated string of folder paths relative to root, defaults to `"components"`

### Package Options

Options can be set in the package.json file under the `"figmental"` prop

```json
package.json
{
  ...
  "figmental": {
    "previewDir": "relative/path/to/a/preview/dir",
    "include": ["components/specific/dir", "other/specific/dir"]
  }
}

```
