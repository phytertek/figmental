const { lstat, readFile, readdir } = require('fs').promises;
const { join } = require('path');

const VUE_EXTENSION = '.vue';
const FILE_ENCODING = 'utf-8';
const FIGMENTAL_COMMENT = '@figmental';
const COMMENT_CLOSE = '-->';
const EMPTY_STRING = '';

const readFigments = async (root, entryPath) => {
    const entries = await readdir(join(root, entryPath));
    let figments = {};
    for (let entry of entries) {
        const entryStatus = await lstat(join(root, entryPath, entry));
        if (entryStatus.isDirectory()) {
            figments = {
                ...figments,
                ...(await readFigments(root, join(entryPath, entry))),
            };
        } else if (entryStatus.isFile()) {
            const fileString = await readFile(
                join(root, entryPath, entry),
                FILE_ENCODING
            );
            const figmentalStart = fileString.indexOf(FIGMENTAL_COMMENT);
            if (figmentalStart > -1) {
                const figment = fileString
                    .slice(
                        figmentalStart,
                        fileString.indexOf(COMMENT_CLOSE, figmentalStart)
                    )
                    .replace(FIGMENTAL_COMMENT, EMPTY_STRING)
                    .replace(COMMENT_CLOSE, EMPTY_STRING)
                    .trim();
                figments[join(root, entryPath, entry)] = {
                    path: join(
                        entryPath,
                        entry.replace(VUE_EXTENSION, EMPTY_STRING)
                    ),
                    name: entry.replace(VUE_EXTENSION, EMPTY_STRING),
                    figment,
                };
            }
        }
    }
    return figments;
};

module.exports = readFigments;
