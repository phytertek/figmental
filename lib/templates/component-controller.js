module.exports = (figments) => `
<template>
  <a-layout>
    <a-layout-sider width="300" v-model="collapsed" collapsible theme="light">
      <a-menu :selectable="false" mode="inline">
        ${Object.values(figments).map(
            (figment) => `
        <a-menu-item key="${figment.path}">
          <nuxt-link to="${figment.name}">
            <a-icon type="build" />
            <span>
              ${figment.path
                  .split('/')
                  .map((c) => `${c.charAt(0).toUpperCase()}${c.slice(1)}`)
                  .join(' ')}
            </span>
          </nuxt-link>
        </a-menu-item>
        `
        )}
      </a-menu>
    </a-layout-sider>
    <a-layout-content>
      <nuxt-child />
    </a-layout-content>
  </a-layout>
</template>

<script>
export default {
  data() {
    return {
      collapsed: true
    };
  }
};
</script>

<style lang="less" scoped>
</style>

`;
