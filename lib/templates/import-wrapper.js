module.exports = (previewControllerPath, previewComponentDirPath) => `
<template>
  <Preview :key="page + 'preview'">
    <component :is="routeComponent" />
  </Preview>
</template>

<script>
import Preview from '@/${previewControllerPath}';
export default {
  layout: function (context) {
    return context.route?.params?.layout || 'default';
  },
  components: {
    Preview
  },
  data() {
    return {
      routeComponent: () => import(\`@/${previewComponentDirPath}/-\${this.$route.params.component}\`),
      page: this.$route.params.component
    };
  }
};
</script>

`;
