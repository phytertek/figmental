const prettier = require('prettier');

module.exports = (path, figment) =>
    prettier.format(
        `
<template>
  <div>
  <h1>{{ name }}</h1>
  <p>{{ description }}</p>
  <${path.slice(path.lastIndexOf('/') + 1)}Preview v-bind="controlledProps" />
  </div>
</template>

<script>
import ${path.slice(path.lastIndexOf('/') + 1)}Preview from '@/${path}';

${prettier.format(
    `
export default {
  components: {
    ${path.slice(path.lastIndexOf('/') + 1)}Preview
  },
  inject: ['controlledProps', 'parseProps'],
  mounted() {
    this.parseProps(this.props);
  },
  data() {
    return ${figment}
  }
}
`,
    {
        parser: 'babel',
        printWidth: 80,
        tabWidth: 2,
        semi: true,
    }
)}
</script>
`,
        {
            parser: 'vue',
            printWidth: 80,
            tabWidth: 2,
            semi: true,
            trailingComma: 'none',
            singleQuote: true,
        }
    );
