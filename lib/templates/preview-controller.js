module.exports = () => `
<template>
  <a-layout hasSider>
    <a-layout-sider theme="light" width="300" style="padding: 1rem;">
      <div class="block">
        <div class="prop-input" v-for="prop in propForm" :key="prop.key">
          <span class="align-center label">{{ prop.key }}</span>

          <a-select
            v-if="prop.input === 'select'"
            class="full-width spaced"
            :default-value="textValues[prop.key]"
            @change="(v) => handleSelect(v, prop.key)"
          >
            <a-select-option
              class="full-width spaced"
              v-for="(opt) in optionValues[prop.key]"
              :key="prop.key+opt"
              :value="opt"
            >{{opt}}</a-select-option>
          </a-select>

          <a-input
            class="full-width spaced"
            v-if="prop.input === 'text' || prop.input === 'number'"
            :value="textValues[prop.key]"
            @change="(e) => handleInput(e.target.value, prop)"
            :type="prop.input"
          />
          <a-switch
            class="full-width spaced"
            v-if="prop.input === 'switch'"
            :checked="controlledProps[prop.key]"
            @click="handleSwitch(prop.key)"
          />
          <a-textarea
            autoSize
            class="full-width spaced"
            v-if="prop.input === 'json'"
            :value="textValues[prop.key]"
            @change="(e) => handleJson(e.target.value, prop)"
          />
        </div>
      </div>
    </a-layout-sider>

    <!-- Component Render Panel -->
    <a-layout-content>
      <div class="block preview-pane">
        <slot />
      </div>
    </a-layout-content>
  </a-layout>
</template>

<script>
export default {
  data() {
    return {
      textValues: {},
      controlledProps: {},
      propForm: [],
      optionValues: {}
    };
  },
  methods: {
    handleInput(value, { key, input }) {
      this.textValues[key] = value;
      this.controlledProps[key] = input === 'number' ? (value !== '' ? +value : undefined) : value;
      this.controlledProps.__ob__.dep.notify();
    },
    handleJson(value, { key }) {
      this.textValues[key] = value;
      try {
        this.controlledProps[key] = JSON.parse(value);
        this.controlledProps.__ob__.dep.notify();
      } catch {}
    },
    handleSwitch(key) {
      console.log('handle switch', key, this.controlledProps[key]);
      this.controlledProps[key] = !this.controlledProps[key];
      this.controlledProps.__ob__.dep.notify();
    },
    handleSelect(value, key) {
      this.controlledProps[key] = value;
      this.textValues[key] = value;
      this.controlledProps.__ob__.dep.notify();
    },
    parseProps(propStructure) {
      console.log('Parse Props', propStructure);
      if (!this.propForm.length) {
        for (let [key, prop] of Object.entries(propStructure)) {
          switch (prop.type) {
            case 'Enum':
              this.propForm.push({ input: 'select', key });
              this.controlledProps[key] = prop.default;
              this.optionValues[key] = prop.enum;
              this.textValues[key] = prop.default;
              break;
            case 'String':
              this.propForm.push({ input: 'text', key });
              this.controlledProps[key] = prop.default;
              this.textValues[key] = prop.default;
              break;
            case 'Number':
              this.propForm.push({ input: 'number', key });
              this.controlledProps[key] = prop.default;
              this.textValues[key] = +prop.default;
              break;
            case 'Boolean':
              this.propForm.push({ input: 'switch', key });
              this.controlledProps[key] = prop.default;
              break;
            case 'Object':
            case 'Array':
              this.propForm.push({ input: 'json', key });
              this.controlledProps[key] = prop.default;
              this.textValues[key] = prop.default
                ? JSON.stringify(prop.default, null, 2)
                : undefined;
              break;
            default:
              break;
          }
        }
        this.controlledProps.__ob__.dep.notify();
      }
    }
  },
  provide() {
    return {
      controlledProps: this.controlledProps,
      parseProps: this.parseProps
    };
  }
};
</script>

<style lang="less" scoped>
.prop-input {
  width: 100%;
}
.full-width {
  width: 100%;
}
.label {
  width: 100%;
  color: @color-midnight;
  font-size: 1rem;
  font-weight: 400;
}
.preview-pane {
  padding: 1.5rem;
}
.block {
  display: block;
}
.spaced {
  padding-bottom: 0.25rem;
}
</style>
`;
