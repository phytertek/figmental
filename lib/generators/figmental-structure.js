const { mkdir, writeFile } = require('fs').promises;
const { existsSync } = require('fs');
const { join } = require('path');
const generateComponentController = require('../templates/component-controller');
const generatePreviewController = require('../templates/preview-controller');
const generateImportWrapper = require('../templates/import-wrapper');

const PREVIEW_CONTROLLER_FILE = '-preview-controller';
const COMPONENT_CONTROLLER_FILE = 'components';
const COMPONENT_WRAPPER_FILE = '_component';
const PREVIEW_COMPS_DIR = 'components';
const VUE_EXT = '.vue';
const FILE_ENCODING = 'utf-8';

const generatepreviewStructure = async (root, previewRoot, figments) => {
    const dirPath = join(root, previewRoot);
    const controllerFileRelativePath = join(
        previewRoot,
        PREVIEW_CONTROLLER_FILE
    );
    const controllerFilePath = join(dirPath, PREVIEW_CONTROLLER_FILE);
    const componentDirRelativePath = join(previewRoot, PREVIEW_COMPS_DIR);
    const componentDirPath = join(dirPath, PREVIEW_COMPS_DIR);
    const componentControllerFilePath = join(
        dirPath,
        COMPONENT_CONTROLLER_FILE
    );
    const componentWrapperPath = join(
        componentDirRelativePath,
        COMPONENT_WRAPPER_FILE
    );
    if (!existsSync(dirPath)) {
        await mkdir(dirPath);
    }

    if (!existsSync(controllerFilePath)) {
        await writeFile(
            controllerFilePath + VUE_EXT,
            generatePreviewController(),
            FILE_ENCODING
        );
    }

    await writeFile(
        componentControllerFilePath + VUE_EXT,
        generateComponentController(figments),
        FILE_ENCODING
    );

    if (!existsSync(componentDirPath)) {
        await mkdir(componentDirPath);
    }

    await writeFile(
        componentWrapperPath + VUE_EXT,
        generateImportWrapper(
            controllerFileRelativePath,
            componentDirRelativePath
        ),
        FILE_ENCODING
    );
};

module.exports = generatepreviewStructure;
