const { writeFile } = require('fs').promises;
const { join } = require('path');
const generatePreview = require('../templates/figment-preview');

const PREVIEW_COMPS_DIR = 'components';
const PREVIEW_COMP_FILENAME = (name) => `-${name}.vue`;
const FILE_ENCODING = 'utf-8';

const generateFigmentPreviews = async (root, previewRoot, figments) => {
    for (let { name, figment, path } of Object.values(figments)) {
        const file = generatePreview(path, figment);
        const previewFilePath = join(
            root,
            previewRoot,
            PREVIEW_COMPS_DIR,
            PREVIEW_COMP_FILENAME(name)
        );
        await writeFile(previewFilePath, file, FILE_ENCODING);
    }
};

module.exports = generateFigmentPreviews;
