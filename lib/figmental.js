#!/usr/bin/env node

const { join } = require('path');
const { readFileSync } = require('fs');
const { rmdir } = require('fs').promises;

const readFigments = require('./readers/read-figments');
const generateFigmentPreviews = require('./generators/generate-figment-previews');
const generatePreviewerStructure = require('./generators/generate-previewer-structure');

const root = process.argv[2] || process.cwd();

const package = JSON.parse(readFileSync(join(root, 'package.json'), 'utf-8'));

const previewRoot =
    process.argv[3] || package.figmental
        ? package.figmental.previewDir
        : false || 'pages/preview';

const folders = process.argv[4]
    ? process.argv[4].split(',').map((s) => s.trim())
    : false || package.figmental
    ? package.figmental.include
    : false || ['components'];

const run = async () => {
    try {
        await rmdir(join(root, previewRoot), { recursive: true });
    } catch {}
    let figments = {};
    for (folder of folders) {
        figments = { ...figments, ...(await readFigments(root, folder)) };
    }
    await generatePreviewerStructure(root, previewRoot, figments);
    await generateFigmentPreviews(root, previewRoot, figments);
};

run();
